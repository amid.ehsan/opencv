import cv2 as cv
import numpy as np
from lib import *

img = cv.imread('.\\Resources\Photos\cats.jpg')
cv.imshow('Park',img)

average = cv.blur(img, (3,3))
cv.imshow("Average Blur",average)

gauss = cv.GaussianBlur(img, (3,3),0)
cv.imshow('Gayssian',gauss)

median = cv.medianBlur(img,3)
cv.imshow('median',median)

bilateral = cv.bilateralFilter(img, 10, 35, 25)
cv.imshow("bilateral",bilateral)

cv.waitKey(0) 