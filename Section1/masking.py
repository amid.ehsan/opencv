import cv2 as cv
import numpy as np
from lib import *

img = cv.imread('.\\Resources\Photos\cats.jpg')
cv.imshow('cat',img)
blank = np.zeros(img.shape[:2], dtype='uint8')
circle = cv.circle(blank.copy(), (200,200), 200,255,-1)
mask = cv.circle(blank, (img.shape[1]//2,img.shape[0]//2), 100,255,-1)
cv.imshow("mask",mask)

masked = cv.bitwise_and(img,img,mask=mask)
cv.imshow("masked",masked)
cv.waitKey(0) 