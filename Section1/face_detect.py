import cv2 as cv
from matplotlib import scale
import numpy as np
from lib import *

img = cv.imread('.\\Resources\Photos\lady.jpg')
cv.imshow('Lacy',img)

gray = cv.cvtColor(img,cv.COLOR_BGR2GRAY)
cv.imshow('gray',gray)

haar_cascade = cv.CascadeClassifier('haar_face.xml')
faces_rect = haar_cascade.detectMultiScale(gray,scaleFactor=1.1,minNeighbors=3)

#print(f'Number of the faces = {len(faces_rect)}')


cv.waitKey(0) 