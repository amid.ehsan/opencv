import cv2 as cv
import numpy as np
from lib import *

img = cv.imread('.\\Resources\Photos\park.jpg')
cv.imshow('Park',img)


#Translation
def translate(img, x, y):
    transMat = np.float32([[1,0,x],[0,1,y]])
    dimenstions = (img.shape[1],img.shape[0])
    return cv.warpAffine(img, transMat,dimenstions)

# negative x --> left
# negative y --> up
# positive x --> right
# possitive y --> down

translated = translate(img , -100,100)
#cv.imshow("translated", translated)

# rotatioin
def rotate(img, angle, rotPoint=None):
    (height,width) = img.shape[:2]

    if rotPoint is None:
        rotPoint = (width//2,height//2)
    
    rotMat = cv.getRotationMatrix2D(rotPoint, angle,1.0)
    dimensions = (width,height)

    return cv.warpAffine(img, rotMat,dimensions)

rotated = rotate(img, 30,(100,200))
cv.imshow("Rotated", rotated)

flip = cv.flip(img,-1)
cv.imshow('Flip', flip)

cropped = img[200:400, 300:400]
cv.imshow("cropped", cropped)



cv.waitKey(0) 