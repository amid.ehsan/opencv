import cv2 as cv
import numpy as np

blank = np.zeros((500, 500, 3), dtype='uint8' )
# cv.imshow('Blank', blank)

blank[200:300, 300:400] = 0,255,0
# cv.imshow('Green', blank)


# rectangle
cv.rectangle(blank, (0, 0), (250,250), (255,255,0), thickness=2 )
# cv.imshow('Rectangle', blank)

# circle
cv.circle(blank, (blank.shape[1]//2, blank.shape[0]//2), 40, (0,0,255),  thickness=3)
# cv.imshow('Circle', blank)

# line
cv.line(blank, (0,0) , (200,400), (255,0,0), thickness= 1)
# cv.imshow('Line', blank)

# text
cv.putText(blank, "Hello", (200,200), cv.FONT_HERSHEY_TRIPLEX, 2, (255,255,255), thickness=2)
cv.imshow('Text', blank)
cv.waitKey(0)
