import cv2 as cv
from cv2 import CHAIN_APPROX_SIMPLE
import numpy as np
import matplotlib.pyplot as plt
from lib import *

img = cv.imread('.\\Resources\Photos\park.jpg')
cv.imshow('park',img)


# plt.imshow(img)
# plt.show()
gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
cv.imshow('gray',gray)

hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)
cv.imshow('hsv', hsv)

lab = cv.cvtColor(img, cv.COLOR_BGR2LAB)
cv.imshow('lab', lab)

hsv_bgr = cv.cvtColor(hsv, cv.COLOR_HSV2BGR)
cv.imshow('hsv_bgr', hsv_bgr)

gray_bgr = cv.cvtColor(gray, cv.COLOR_GRAY2BGR)
cv.imshow('gray_bgr', gray_bgr)


rgb = cv.cvtColor(img, cv.COLOR_BGR2RGB)
cv.imshow('rgb', rgb)


cv.waitKey(0) 