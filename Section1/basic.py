import cv2 as cv
from lib import *

img = cv.imread('.\\Resources\Photos\park.jpg')
cv.imshow('Park',img)

# gray
gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
# cv.imshow('gray', gray)

# Blur
blur = cv.GaussianBlur(img, (7,7), cv.BORDER_DEFAULT)
# cv.imshow('Blur', blur)

# Edge Cascade
canny = cv.Canny(blur, 125, 175)
cv.imshow('Canny', canny)

dilated = cv.dilate(canny, (7,7), iterations=3)
cv.imshow('Dilated', dilated)

eroded = cv.erode(dilated, (7,7), iterations=3)
cv.imshow('Eroded', eroded)

resized = cv.resize(img, (500,500), interpolation=cv.INTER_CUBIC)
cv.imshow("Resized",resized)

cropped = img[50:400, 300:600]
cv.imshow("Cropped",cropped)
cv.waitKey(0)

