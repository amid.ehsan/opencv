import cv2 as cv


def rescaleFrame(frame, scale=0.75):
    # images, Videos and Live Videos
    width = int(frame.shape[1] * scale)
    height = int(frame.shape[0] * scale)
    dimension = (width, height)
    return cv.resize(frame, dimension, interpolation=cv.INTER_AREA)

def changeRes(width, hieght):
    # Live Videos
    capture.set(3, height)
    capture.set(4, height)
