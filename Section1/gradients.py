import cv2 as cv
import numpy as np
from lib import *

img = cv.imread('.\\Resources\Photos\park.jpg')
cv.imshow('Cats',img)

gray = cv.cvtColor(img,cv.COLOR_BGR2GRAY)
cv.imshow("gray",gray)

lap = cv.Laplacian(gray, cv.CV_64F)
lap = np.uint8(np.absolute(lap))
cv.imshow('Laplacian',lap)

sobelx = cv.Sobel(gray, cv.CV_64F,1,0)
sobely = cv.Sobel(gray, cv.CV_64F,0,1)
cv.imshow('subelx',sobelx)
cv.imshow('subely',sobely)

combined_soble = cv.bitwise_or(sobelx,sobely)

cv.imshow('combined_soble',combined_soble)

canny = cv.Canny(gray,150,175)
cv.imshow('canny',canny)


cv.waitKey(0) 